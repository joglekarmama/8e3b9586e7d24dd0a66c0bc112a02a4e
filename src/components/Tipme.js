import React, { useState, useEffect } from "react";
import config from 'visual-config-exposer';
import { connectToParent } from "penpal";
import './tipme.css'

const username = process.env["userid"] || "userid";
const userPublicAddress =
  process.env["userPublicAddress"] || "userPublicAddress";
let connection;
const headline = config.settings.headline;
console.log(headline);
const Mob_headline = config.settings.Mob_headline;
const success_header = config.settings.success_header;
const success_text = config.settings.success_text;
const success_buttonText = config.settings.success_buttonText;
const success_image = config.settings.success_image;
const title = config.settings.title;
const enough_funds = config.settings.enough_funds;
let Price = config.settings.Price;
function isLoadedWithinIframe() {
  try {
    // verify the access to window.top object is succesfull.
    window.top.origin;
    return false;
  } catch (e) {
    console.log("window.top error");
    return process.env.NODE_ENV === "production";
  }
}

if (isLoadedWithinIframe()) {
  connection = connectToParent({
    // Methods child is exposing to parent
    methods: {},
  });
}
export default function Tipme() {



  // const router = useRouter()
  function unblur() {
    let amountInput = document.getElementById('amountInput').value;
    let isValidate = true;
    // let isValidate = false;
    // if (!amountInput) {
    //   document.getElementsByClassName("fund-alert")[0].classList.remove("fade");
    // } else {
    //   isValidate = true;
    //   document.getElementsByClassName("fund-alert")[0].classList.add("fade");
    // }

    if (isValidate === true) {
      config.settings.Price = document.getElementById('amountInput').value;
      if (isLoadedWithinIframe()) {
        let transactionData = {
          amount: document.getElementById('amountInput').value,
          recipient: {
            publicAddress: userPublicAddress,
            user: {
              name: username,
            },
          },
          metadata: {
            show: { MEMO: config.settings.headline },
          },
        };
        connection.promise.then((parent) => {
          parent
            .initiateTransaction(transactionData)
            .then((accept) => {
              console.log("child print accept", accept);
              revealAsset();
            })
            .catch((reject) => {
              console.log("child print reject", reject);
            });
        });
      } else {
        revealAsset();
      }
    }

    //config.description=document.getElementById('leave-note').value;

    function revealAsset() {
      setTimeout(() => {
        document.getElementsByClassName("success-container")[0].classList.add("display-b");
        document.getElementsByClassName("success-container")[0].classList.remove("display-n");
        document.getElementsByClassName("home-container")[0].classList.add("display-n");
        document.getElementsByClassName("home-container")[0].classList.remove("display-b");

        document.getElementById('tipvalue').innerHTML = config.settings.Price + ' ' + success_header
        //Price= config.settings.Price;
        //home-container
        //router.push('/success')
      }, 500);
    }


  }

  return (
    <>

      <div className="success-container display-n">
        <div className="container h-100">
          <div className="row success-row justify-content-center align-items-center">
            <div className="row justify-content-center align-items-center w-100" >
              <div className="col-12 text-center" >
                <p className="success-text success-header mb-4" id="tipvalue">{config.settings.Price} {success_header} </p>
              </div>
              <div className="col-lg-12 text-center">
                <img className="success-image" src={success_image}></img>
              </div>
              <div className="col-12 col-md-10 text-center mt-5" >
                <p className="success-text">{success_text}</p>
                <a href="/"><button className="btn send-tip-again-btn mt-3"  >{success_buttonText}</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="home-container display-b" style={{
          backgroundImage: "radial-gradient(107.79% 107.79% at 50% 50%, rgba(0, 0, 0, 0.0001) 0%, #000000 100%),url(" +  config.settings.bg_image  + ")",
        }} >
        <div className="container-fluid h-100">
          <div className="row tip-row  justify-content-center align-items-center ">
            <div className="col-12 col-md-6 text-left ">
              <div className="alert alert-danger alert-dismissible fade fund-alert" role="alert">
                <strong>{enough_funds}</strong>
                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <p className="send-tip-text d-none d-md-block text-center">{headline}</p>
              <p className="send-tip-text d-block d-md-none text-center mobile-header">{Mob_headline} </p>
              <form>
                <div className="card send-tip-card">

                  <div className="form-group">
                    <label htmlFor="amountInput">Amount</label>
                    <input type="number" name="amount" className="form-control input-box form-control-lg" value={config.settings.Price} id="amountInput" placeholder="Enter amount" required />
                  </div>
                  <div className="form-group">
                    <label htmlFor="leave-note">Leave a note</label>
                    <textarea className="form-control input-box" id="leave-note" placeholder="Thank you for starting such a great initiative. I hope my small contribution helps in some way. " rows="3"></textarea>
                  </div>
                  <div className="form-group">
                    <input type="button" onClick={unblur} className="btn send-tip-button d-none d-md-block form-control-lg" value="SEND TIP" />
                    <input type="button" onClick={unblur} className="btn send-tip-button btn-mobile d-block d-md-none" value="SEND TIP" />
                  </div>
                </div>

              </form>

            </div>
          </div>
        </div>
      </div>


      {/* <div className="donation-container">
        <div className="donation-box">
          <div className="title">{config.settings.title}</div>
          <div className="fields">
            <input type="text" id="firstName" placeholder="First Name" />
            <input type="text" id="lastName" placeholder="Last Name" />
            <input type="text" id="email" placeholder="Email" />
          </div>
          <div className="amount">
            <div className="button">$30</div>
            <div className="button">$50</div>
            <div className="button">$100</div>
            <div className="button">$<input type="number" className="set-amount" placeholder="" /> </div>
          </div>
          <div className="checkboxes">
            <input type="checkbox" id="receipt" className="checkbox" />
            <label htmlFor="receipt">Email Me A Receipt</label>
            <br />
            <input type="checkbox" id="anon" className="checkbox" />
            <label htmlFor="anon">Give Anonymously</label>
            <br />
            <input type="checkbox" id="list" className="checkbox" />
            <label htmlFor="list">Add Me To Email List</label>
          </div>
          <div className="confirm">
          </div>
          <div className="donate-button" onClick={unblur}><i className="fa fa-heart"></i> Donate Now</div>
        </div>
      </div> */}
    </>
  );
}
// export default Tipme;
